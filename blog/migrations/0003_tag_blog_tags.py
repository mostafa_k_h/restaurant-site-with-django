# Generated by Django 4.0.4 on 2022-05-20 15:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_alter_blog_category_alter_category_published_at'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50, verbose_name='عنوان ')),
                ('slug', models.SlugField(verbose_name='عنوان لاتین ')),
                ('published_at', models.DateTimeField(auto_now_add=True, verbose_name='تاریخ انتشار')),
                ('update_at', models.DateTimeField(auto_now=True, verbose_name='تاریخ به روز رسانی')),
            ],
        ),
        migrations.AddField(
            model_name='blog',
            name='tags',
            field=models.ManyToManyField(blank=True, null=True, related_name='blogs', to='blog.category', verbose_name='تگ ها'),
        ),
    ]
