
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext as _
from django.contrib.auth.models import User

# Create your models here.

class Blog(models.Model):
    title = models.CharField(_("عنوان"),max_length=50)
    description = models.CharField(_("توضیحات"),max_length=200)
    content = models.TextField(_("متن"))
    created_at = models.DateTimeField(_("زمان انتشار"),default=timezone.now)
    author = models.ForeignKey(User , verbose_name="نویسنده",on_delete=models.CASCADE)
    image = models.ImageField(_("تصویر"), upload_to="blogs/", blank=True,null=True)
    category = models.ForeignKey("Category", verbose_name=_(" دسته بندی "), on_delete=models.CASCADE,related_name="blog")
    tags = models.ManyToManyField("Category", verbose_name=_("تگ ها"),null=True,blank=True,related_name="blogs")

    def __str__(self): 
           return self.title

class Category(models.Model):
    title = models.CharField(_("عنوان "),max_length=50)
    slug = models.SlugField(_("عنوان لاتین "),max_length=50)
    published_at = models.DateTimeField(_("تاریخ انتشار"),auto_now_add=True,auto_now=False)

    def __str__(self): 
           return self.title


class Tag(models.Model):
    title = models.CharField(_("عنوان "),max_length=50)
    slug = models.SlugField(_("عنوان لاتین "),max_length=50)
    published_at = models.DateTimeField(_("تاریخ انتشار"),auto_now_add=True,auto_now=False)
    update_at = models.DateTimeField(_("تاریخ به روز رسانی"),auto_now_add=False,auto_now=True)


    def __str__(self): 
           return self.title


class Comments(models.Model):
    blog = models.ForeignKey("Blog" , verbose_name="مقاله",related_name="comments",on_delete=models.CASCADE)
    name = models.CharField(_("نام کاربر"), max_length=100)
    email = models.EmailField(_("پست الکترونیکی"), max_length=254)
    message = models.TextField(_("متن نظر"))
    date = models.DateField(_("تاریخ انتشار"), auto_now=False , auto_now_add=True)


    def __str__(self):
           return self.email