# Generated by Django 4.0.4 on 2022-05-16 07:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('foods', '0003_food_type_food'),
    ]

    operations = [
        migrations.AlterField(
            model_name='food',
            name='type_food',
            field=models.CharField(choices=[('breakefast', 'صبحانه'), ('drinks', 'نوشیدنی'), ('dinner', 'ناهار'), ('lunch', 'شام')], default='drinks', max_length=10, verbose_name='نوع غذا'),
        ),
    ]
